package com.rpgen.draft;

import com.rpgen.mvc.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class PostSender {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            String uri = new String("http://139.59.172.51:8080/ReportsGenerator/core/register");

            User u = new User();
            u.setFirstName("John");
            u.setLastName("Smith");
            u.setLogin("fooX");
            u.setPassword("barX");
            u.setEmail("jsjs@foo.com");

//            String returns = rt.postForObject(uri, u, String.class);
            ResponseEntity <String> response = rt.postForObject(uri, u, ResponseEntity.class );
            System.out.println("status: " + response.getStatusCode());

        } catch (HttpClientErrorException e) {
            System.out.println("error: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
        }
    }
}
